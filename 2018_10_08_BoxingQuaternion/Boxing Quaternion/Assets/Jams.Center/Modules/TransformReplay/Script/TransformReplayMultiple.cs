﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TransformReplay))]
public class TransformReplayMultiple : MonoBehaviour {

    public TextAsset [] m_textAsset;

    private TransformReplay m_replay;

    public bool m_usedNormalized;
    // Use this for initialization
    void Awake () {

        m_replay = GetComponent<TransformReplay>();
        m_replay.m_onFinish.AddListener(ResetNextOne);
        ResetNextOne();

    }
    public int m_index;
    public enum IndexType { Sequence, Random}
    public IndexType m_indexType;
    private void ResetNextOne()
    {
        if (m_indexType == IndexType.Sequence)
        {

            m_indexType++;
            if (m_index >= m_textAsset.Length)
                m_index = 0;
        }
        if (m_indexType == IndexType.Random)
        {

            m_index = UnityEngine.Random.Range(0, m_textAsset.Length);
        }
        FramesRecord record = FramesRecord.LoadFrom(m_textAsset[m_index].text);
        if(m_usedNormalized)
            record.Normalized();
        m_replay.m_framesRecord = record;
        m_replay.Reset();
    }
}
